package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import javafx.scene.control.Button;

public class MovieProfile3 {

    @FXML
    Button backBtn1;
    @FXML Button BookingBtn1;
    @FXML
    Button threatreHD,threatre4K;

    @FXML
    public void handlebackBtn1Action(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/home.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handlethreatreHD(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking3.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handlethreatre4K(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking24k3.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }
}
