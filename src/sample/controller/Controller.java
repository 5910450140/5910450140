package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;

public class Controller {
    @FXML
    Button goBtn;
    @FXML
    Button signUpBtn;
    @FXML
    TextField username_textfield;
    @FXML
    PasswordField password_textfield;

    ArrayList<Storage> storages;

    public void initialize() throws IOException {
        storages = new ArrayList<>();
        File file = new File("registerdata.txt");
        String path = file.getAbsolutePath();
        System.out.println(path);
        BufferedReader br = null;
        String filename = System.getProperty("user.dir") + File.separator + "registerdata.txt";
        System.out.println(filename);
        try {
            br = new BufferedReader(new FileReader(filename));
            String line = "";
            while ((line = br.readLine()) != null) {
                String[] profile = line.split(",");
                String name = profile[0];
                String surname = profile[1];
                String email = profile[2];
                String telnum = profile[3];
                String username = profile[4];
                String password = profile[5];
                storages.add(new Storage(name, surname, email, telnum, username, password));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    @FXML
    public void handelNextBtnAction(ActionEvent event) throws IOException {
        if (password_textfield.getText().isEmpty() && username_textfield.getText().isEmpty()) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Fill the box.");
            alert.setContentText("Please fill username and password correctly!");
            alert.showAndWait();

        }
        else if (password_textfield.getText().isEmpty()) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Please fill password correctly!");
            alert.showAndWait();
        }
        else if (username_textfield.getText().isEmpty()) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Please fill username correctly!");
            alert.showAndWait();

        } else {
            boolean status = false;
            for (Storage x : storages) {
                if (x.getUsername().equals(username_textfield.getText()) && x.getPassword().equals(password_textfield.getText())) {
                    status = true;
                    break;

                }
            }
            if (status) {
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/home.fxml"));
                String x = username_textfield.getText();
                stage.setScene(new Scene(loader.load(), 800, 600));
                System.out.println(x);
                Home controller = loader.getController();
                controller.setName(x);
                stage.show();
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("WRONG USERNAME OR PASSWORD.");
                alert.showAndWait();

            }
        }

    }
    @FXML
    public void handelSignUpBtnAction(ActionEvent event)throws  IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/register.fxml"));
        stage.setScene(new Scene(loader.load(),480,600));
        stage.show();
    }
}

