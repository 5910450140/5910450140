package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SignUp {
    @FXML
    TextField name_textfield, surname_textfield, username_textfield, email_textfield, telnum_textfield;
    @FXML
    PasswordField password_textfield;
    @FXML
    Button backBtn, submit;

    ArrayList<Storage> storages;

    public void initialize() throws IOException {

        storages = new ArrayList<>();
        File file = new File("registerdata.txt");
        String path = file.getAbsolutePath();
        System.out.println(path);
        BufferedReader br = null;
        String filename = System.getProperty("user.dir") + File.separator + "registerdata.txt";
        System.out.println(filename);
        try {
            br = new BufferedReader(new FileReader(filename));
            String line = "";
            while ((line = br.readLine()) != null) {
                String[] profile = line.split(",");
                String name = profile[0];
                String surname = profile[1];
                String email = profile[2];
                String telnum = profile[3];
                String username = profile[4];
                String password = profile[5];
                storages.add(new Storage(name, surname, email, telnum, username, password));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

//        if (br != null) {
//            String st;
//            while ((st = br.readLine()) != null) {
//                String[] profile = st.split(",");
//                String name = profile[0];
//                String surname = profile[1];
//                String email = profile[2];
//                String telnum = profile[3];
//                String username = profile[4];
//                String password = profile[5];
//                System.out.println(st +"KUY");
//                storages.add(new Storage(name, surname, email, telnum, username, password));
//            }
//        }
//        System.out.println(storages.size());

    }
    @FXML
    public void submitRegisterBtnAction(ActionEvent event) throws IOException {
        if (name_textfield.getText().isEmpty() || surname_textfield.getText().isEmpty() || username_textfield.getText().isEmpty() || email_textfield.getText().isEmpty()
                || telnum_textfield.getText().isEmpty()) {
            Alert msg = new Alert(Alert.AlertType.INFORMATION);
            msg.setTitle(email_textfield.getText());
            msg.setContentText("Please fill all the blank !!");
            msg.showAndWait();

        }
        else {
            boolean status = true;
            for (Storage x : storages) {
                System.out.println(x.toString());
                if (x.getUsername().equals(username_textfield.getText()) || x.getEmail().equals(email_textfield.getText())) {
                    if(x.getEmail().equals(email_textfield.getText())){
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("FAILED");
                        alert.setContentText("please use another e-mail");
                        alert.showAndWait();
                    }
                    else if(x.getUsername().equals(username_textfield.getText())){
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("FAILED");
                        alert.setContentText("please use another username");
                        alert.showAndWait();
                    }
                    status = false;
                    break;
                }
            }
            if (status) {
                StringBuilder sb = new StringBuilder();
                sb.append(name_textfield.getText().toString() + ",");
                sb.append(surname_textfield.getText().toString() + ",");
                sb.append(email_textfield.getText().toString() + ",");
                sb.append(telnum_textfield.getText().toString() + ",");
                sb.append(username_textfield.getText().toString() + ",");
                sb.append(password_textfield.getText().toString() + ",\n");
                File file = new File("registerdata.txt");
                FileWriter writer = new FileWriter(file, true);
                writer.write(sb.toString());
                writer.close();



                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("SUCCESS");
                alert.setContentText("Successfully registered.");
                alert.showAndWait();


                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../Loginfix.fxml"));
                stage.setScene(new Scene(loader.load(),480,600));
                stage.show();
            }
        }
    }


    @FXML
    public void handelBackBtnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Loginfix.fxml"));
        stage.setScene(new Scene(loader.load(),480,600));
        stage.show();
    }
}

//
//                    InputStreamReader inReader = new InputStreamReader(System.in);
//                    BufferedReader buffer = new BufferedReader(inReader);
//
//                    PrintWriter out = new PrintWriter(fileWriter);
//
//
//                    fileWriter.write(name_textfield.getText() + ",");
//                    fileWriter.write(surname_textfield.getText() + ",");
//                    fileWriter.write(email_textfield.getText() + ",");
//                    fileWriter.write(username_textfield.getText() + ",");
//                    fileWriter.write(password_textfield.getText() + "\n");
//                    fileWriter.flush();
//                    fileWriter.close();
//
//                } catch (IOException e) {
//                    System.err.println("Error reading from user");
//                }
//
//                signup = (Button) a.getSource();
//                Stage stage = (Stage) signup.getScene().getWindow();
//                FXMLLoader loader = new FXMLLoader(getClass().getResource("WelcomePage.fxml"));
//
//                try {
//                    stage.setScene(new Scene(loader.load(), 600, 400));
//                    stage.centerOnScreen();
//                    stage.show();
//
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }

//
//
//            StringBuilder sb = new StringBuilder();
//            sb.append(name_textfield.getText().toString()+",");
//            sb.append(surname_textfield.getText().toString()+",");
//            sb.append(email_textfield.getText().toString()+",");
//            sb.append(telnum_textfield.getText().toString()+",");
//            sb.append(username_textfield.getText().toString()+",");
//            sb.append(password_textfield.getText().toString()+",\n");
//            File file = new File("registerdata.txt");
//            FileWriter writer = new FileWriter(file,true);
//            writer.write(sb.toString());
//            writer.close();
//        if(!name.getText().isEmpty() && !surname.getText().isEmpty() && !username_textfield.getText().isEmpty() && !email_textfield.getText().isEmpty()
//        && !telnum.getText().isEmpty()){
//            //check is register or not.
//            Path path = Paths.get("registerdata.txt");
//            long count = Files.lines(path).count();
//            for (int i=0;i<count;i++ ){
//                String line = Files.readAllLines(path).get(i);
//                if(!line.trim().equals("")){
//                    String[] profile = line.split(",");
//
//                    String name = profile[0];
//                    String surname = profile[1];
//                    String email = profile[2];
//                    String username = profile[3];
//                    String password =  profile[4];
//                    String telnum =  profile[5];
//
//                    //Email Matched!
//                    if(profile[3].equals(username_textfield.getText()))
//                    { //Note trim() method remove space from front and behind of string if there is any
//                        //Now checking if password match
//                        System.out.println("1");
//                        Alert msg = new Alert(Alert.AlertType.CONFIRMATION);
//                        msg.setTitle(email_textfield.getText());
//                        msg.setContentText("You can't use this Username !!");
//                        msg.showAndWait();
//
//                        break; //Email match and pass match, Close loop
//                    }
//                    if(profile[2].equals(email_textfield.getText()) || profile[3].equals(username_textfield.getText()))
//                    {
//                        System.out.println("1111");
//                        Alert alert = new Alert(Alert.AlertType.ERROR);
//                        alert.setTitle(email_textfield.getText());
//                        alert.setContentText("You can't use this Username Or Email");
//                        alert.showAndWait();
//
//                        break; //Email match and pass match, Close loop
//
//                    }
//                    else if(!(profile[2].equals(email_textfield.getText())) && !(profile[3].equals(username_textfield.getText()))){
//
//                        System.out.println("11111111111");
//
//                        File file = new File("registerdata.txt");
//                        FileWriter writer = new FileWriter(file,true);
//                        writer.write(sb.toString());
//                        writer.close();
//
//                        Alert msg = new Alert(Alert.AlertType.INFORMATION);
//                        msg.setTitle(username_textfield.getText());
//                        msg.setContentText("Register Successful." +
//                                "please go back to login.");
//                        msg.showAndWait();
//
//                        break;
//
//                    }
//                }
//            }
//            sb.append(name.getText().toString()+",");
//            sb.append(surname.getText().toString()+",");
//            sb.append(email.getText().toString()+",");
//            sb.append(telnum.getText().toString()+",");
//            sb.append(username.getText().toString()+",");
//            sb.append(password.getText().toString()+",\n");
//
//            File file = new File("registerdata.txt");
//            FileWriter writer = new FileWriter(file,true);
//            writer.write(sb.toString());
//            writer.close();
//
//            Alert msg = new Alert(Alert.AlertType.INFORMATION);
//            msg.setTitle(username.getText());
//            msg.setContentText("Register Successful." +
//                    "please go back to login.");
//            msg.showAndWait();
//
//        }
//        else{
//            Alert msg = new Alert(Alert.AlertType.ERROR);
//            msg.setTitle(username_textfield.getText());
//            msg.setContentText("Please input the information.");
//            msg.showAndWait();
//        }

//        Path path = Paths.get("registerdata.txt");
//        long count = Files.lines(path).count();
//        for (int i=0;i<count;i++ ) {
//            String line = Files.readAllLines(path).get(i);
//            String[] profile = line.split(",");
//            String name = profile[0];
//            String surname = profile[1];
//            String email = profile[2];
//            String telnum = profile[3];
//            String username = profile[4];
//            String password = profile[5];
//            storages.add(new Storage(name,surname,email,telnum,username,password));
//        }
//
//
