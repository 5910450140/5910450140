package sample.controller;

public class Storage {
    private String Name;
    private String Surname;
    private String Email;
    private String Telnum;
    private String Username;
    private String Password;

    public Storage(String name,String surname,String email,String telnum,String username,String password) {
        this.Name = name;
        this.Surname = surname;
        this.Email = email;
        this.Telnum = telnum;
        this.Username = username;
        this.Password = password;
    }

    public String getName() {
        return Name;
    }

    public  void setName(String name) {
        Name = name;
    }

    public  String getSurname() {
        return Surname;
    }

    public  void setSurname(String surname) {
        Surname = surname;
    }

    public  String getEmail() {
        return Email;
    }

    public  void setEmail(String email) {
        Email = email;
    }

    public  String getTelnum() {
        return Telnum;
    }

    public  void setTelnum(String telnum) {
        Telnum = telnum;
    }

    public  String getUsername() {
        return Username;
    }

    public  void setUsername(String username) {
        Username = username;
    }

    public  String getPassword() {
        return Password;
    }

    public  void setPassword(String password) {
        Password = password;
    }

    @Override
    public String toString() {
        return "Storage{" +
                "Name='" + Name + '\'' +
                ", Surname='" + Surname + '\'' +
                ", Email='" + Email + '\'' +
                ", Telnum='" + Telnum + '\'' +
                ", Username='" + Username + '\'' +
                ", Password='" + Password + '\'' +
                '}';
    }
}
