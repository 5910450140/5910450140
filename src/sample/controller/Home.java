package sample.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URISyntaxException;

public class Home {
    @FXML Button MovieProfileBtn1;
    @FXML Button MovieProfileBtn2;
    @FXML Button MovieProfileBtn3;
    @FXML Button MovieProfileBtn4;
    @FXML Button logoutBtn;
    @FXML Button editBy;
    @FXML Label namelabel;

    String name ;

    public void handleMovieProfileBtn1Action(ActionEvent event)throws  IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Movieprofile1.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handleMovieProfileBtn2Action(ActionEvent event)throws  IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Movieprofile2.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handleMovieProfileBtn3Action(ActionEvent event)throws  IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Movieprofile3.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handleMovieProfileBtn4Action(ActionEvent event)throws  IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Movieprofile4.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }



    @FXML
    public void handlebookingBtnAction(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Loginfix.fxml"));
        stage.setScene(new Scene(loader.load(),481,600));
        stage.show();
    }
    @FXML
    public void handleCreatorBtnAction(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/profile.fxml"));
        stage.setScene(new Scene(loader.load(),800,450));
        stage.show();
    }










    public void setName(String name){
        System.out.println(name);
        this.name = name;
    }

    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try{
                    namelabel.setText(name);
                }catch (Exception e){
                }
            }
        });
    }
}
