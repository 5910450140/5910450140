package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;


import java.io.IOException;

public class MovieProfile1 {
    @FXML
    Button backBtn1;
    @FXML Button bookingBtn;
    @FXML Button threatreHD,threatre4K,threatrepreHD,threatrepre4K;

    @FXML
    public void handlebackBtn1Action(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/home.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handlethreatreHD(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking1.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handlethreatre4K(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking24k.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }
    public void handlethretrepreHD(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/bookinghdpre.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }
    public void handlethretrepre4K(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking24kpre.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

}
