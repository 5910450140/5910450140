package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;

public class MovieProfile4 {
    @FXML
    Button backBtn1;
    @FXML Button BookingBtn1;

    @FXML
    public void handlebackBtn1Action(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/home.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handlebookingBtn1Action(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking1.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }
}
