package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import javafx.scene.control.Button;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.Main;

import java.io.*;
import java.util.ArrayList;
import java.io.IOException;

public class booking24k3 {
    @FXML
    Button backBtn;
    @FXML Button bookhdbtn;
    @FXML Button bookHdprebtn;
    @FXML Button book4kprebtn;
    @FXML Button submitBtn;


    @FXML
    ImageView v1,v2,v3,v4,v5,v6,v7,v8;
    @FXML
    ImageView a1,a2,a3,a4,a5,a6,a7,a8;
    @FXML
    ImageView b1,b2,b3,b4,b5,b6,b7,b8;
    @FXML
    ImageView c1,c2,c3,c4,c5,c6,c7,c8;
    @FXML
    ImageView d1,d2,d3,d4,d5,d6,d7,d8;


    private ArrayList<Seat> seats = new ArrayList<>();

    public void initialize(){
        insertSeat();
        testread();
    }

    public void insertSeat(){
        seats.add(new Seat(v1));
        seats.add(new Seat(v2));
        seats.add(new Seat(v3));
        seats.add(new Seat(v4));
        seats.add(new Seat(v5));
        seats.add(new Seat(v6));
        seats.add(new Seat(v7));
        seats.add(new Seat(v8));
        //---------------------
        seats.add(new Seat(a1));
        seats.add(new Seat(a2));
        seats.add(new Seat(a3));
        seats.add(new Seat(a4));
        seats.add(new Seat(a5));
        seats.add(new Seat(a6));
        seats.add(new Seat(a7));
        seats.add(new Seat(a8));
        //---------------------
        seats.add(new Seat(b1));
        seats.add(new Seat(b2));
        seats.add(new Seat(b3));
        seats.add(new Seat(b4));
        seats.add(new Seat(b5));
        seats.add(new Seat(b6));
        seats.add(new Seat(b7));
        seats.add(new Seat(b8));
        //---------------------
        seats.add(new Seat(c1));
        seats.add(new Seat(c2));
        seats.add(new Seat(c3));
        seats.add(new Seat(c4));
        seats.add(new Seat(c5));
        seats.add(new Seat(c6));
        seats.add(new Seat(c7));
        seats.add(new Seat(c8));
        //---------------------
        seats.add(new Seat(d1));
        seats.add(new Seat(d2));
        seats.add(new Seat(d3));
        seats.add(new Seat(d4));
        seats.add(new Seat(d5));
        seats.add(new Seat(d6));
        seats.add(new Seat(d7));
        seats.add(new Seat(d8));


    }

    public void testread(){
        try {

            File file = new File("out/production/projectsjava/bookingdata2-3.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader br = new BufferedReader(fileReader);
//            File file = new File("bookingdata1.txt");
//            FileReader fileReader = new FileReader(file);
//          BufferedReader br = new BufferedReader(new FileReader);
//            BufferedReader br = new BufferedReader(fileReader);
            int priceSeat;
            int checkSeat;
            int i=0;
            String nameSeat;
            String line = "";
            while ((line = br.readLine()) != null){
                String[] spliter = line.split(",");
                nameSeat = spliter[0];
                priceSeat = Integer.parseInt(spliter[1]);
                checkSeat = Integer.parseInt(spliter[2]);
                seats.get(i).seatInformation(nameSeat,priceSeat,checkSeat);
                //can't move
                int seatUpdate = i;
                //locked !!!!
                seats.get(i).getImage().setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        seats.get(seatUpdate).setSituation();
                    }
                });
                System.out.println(line + "test");
                i++;
            }
            //close zone
            fileReader.close();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();

        }
    }

    public void handleSubmitBtnAction(ActionEvent event)throws IOException{
        System.out.println("YO");
        String bookingseat = "";
        String seatName = "";
        String nextline = "\n";
        int showPrice= 0;
        File file = new File("out/production/projectsjava/bookingdata2-3.txt");
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter br = new BufferedWriter(fileWriter);

        for (Seat x:seats){
            if(x.getSituation() == 2){
                bookingseat = x.getSeatposition()+","+x.getPrice()+","+0+nextline;
                showPrice += x.getPrice();
                seatName += ": " + x.getSeatposition();
                System.out.println("seat is" + x.getSeatposition());
            }
            else if(x.getSituation() == 9){
                bookingseat = x.getSeatposition()+","+x.getPrice()+","+7+nextline;
                showPrice += x.getPrice();
                seatName += ": " + x.getSeatposition();
                System.out.println("seat is" + x.getSeatposition());
            }
            else{
                bookingseat = x.getSeatposition()+","+x.getPrice()+","+x.getSituation()+nextline;
            }
            br.write(bookingseat);
        }
        br.close();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Booking Seat");
        alert.setContentText("Seat  " + seatName + "\n" +"Price : " + showPrice + "");
        alert.showAndWait();

        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking24k3.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();


    }


    @FXML
    public void handlebackBtn1Action(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/home.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

}
