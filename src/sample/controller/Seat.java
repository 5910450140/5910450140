package sample.controller;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class Seat {
    ImageView image;
    int price;
    String seatposition;
    int situation;

    public Seat(ImageView image) {
        this.image = image;
    }
    //Seat that can't book .
    public void unavailableSeat(){
        if(situation == 0){
            this.image.setImage(new Image("/Img/booking.png"));
        }
        else if(situation == 7){
            this.image.setImage(new Image("/Img/vipbooked.png"));
        }
    }

    public void seatInformation(String seatposition, int price,int situation){
        this.seatposition = seatposition;
        this.price = price;
        this.situation = situation;
        unavailableSeat();
    }

    public ImageView getImage() {
        return image;
    }

    public int getPrice() {
        return price;
    }

    public String getSeatposition() {
        return seatposition;
    }

    public int getSituation() {
        return situation;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setSeatposition(String seatposition) {
        this.seatposition = seatposition;
    }

    public void setSituation() {
        if(situation == 1){
            this.image.setImage(new Image("/Img/book.png"));
            System.out.println("Booking....");
            System.out.println(getSeatposition());
            situation = 2;
        }
        else if(situation== 2){
            this.image.setImage(new Image("/Img/nobook.png"));
            System.out.println("Unbooking....");
            situation = 1;
        }
        //vip zone
        else if(situation== 8){
            this.image.setImage(new Image("/Img/vipbook.png"));
            System.out.println("booking....");
            situation = 9;
        }
        else if(situation== 9){
            this.image.setImage(new Image("/Img/vipnobook.png"));
            System.out.println("Unbooking....");
            situation = 8;
        }

    }
//    public ArrayList<> showSeatsPrice(){
//
//    }
//    public Map<String ,int> showSeatsPrice(){
//        return
//    }
}
