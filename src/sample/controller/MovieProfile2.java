package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Button;


import java.io.IOException;

public class MovieProfile2 {
    @FXML
    Button backBtn2;
    @FXML Button BookingBtn1;
    @FXML
    Button threatreHD,threatre4K;

    @FXML
    public void handlebackBtn2Action(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/home.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handlethreatreHD(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking24k2.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }

    public void handlethreatre4K(ActionEvent event)throws IOException {
        javafx.scene.control.Button b = (javafx.scene.control.Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/booking24k2.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        stage.show();
    }
}
